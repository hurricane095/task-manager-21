package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if(userId == null || userId.isEmpty()) return Collections.emptyList();
        return models
                .stream()
                .filter(model -> projectId.equals(model.getProjectId()) &&
                        userId.equals(model.getUserId()))
                .collect(Collectors.toList());
    }

}
