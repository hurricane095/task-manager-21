package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * UserRepository
 *
 * @author Aleksey_Krivotulov
 */
public class UserRepository extends AbstractRepository<User> implements IUserRepository {


    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }


    @Override
    public User findByLogin(String login) {
        return models
                .stream()
                .filter(model -> login.equals(model.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(String email) {
        return models
                .stream()
                .filter(model -> email.equals(model.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return findByEmail(email) != null;
    }

}
