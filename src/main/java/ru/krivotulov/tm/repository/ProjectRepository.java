package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
