package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IUserOwnedRepository;
import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * AbstractUserRepository
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public M add(final String userId, M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(filterById(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(model -> id.equals(model.getId()) && userId.equals(model.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M delete(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return deleteById(userId, model.getUserId());
    }

    @Override
    public M deleteById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M deleteByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(final String userId) {
        return findAll(userId).size();
    }

}
