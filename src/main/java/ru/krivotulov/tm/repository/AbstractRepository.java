package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IRepository;
import ru.krivotulov.tm.model.AbstractModel;
import ru.krivotulov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * AbstractRepository
 *
 * @author Aleksey_Krivotulov
 */
public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected List<M> models = new ArrayList<>();

    protected Predicate<M> filterById(final String id){
        return model -> id.equals(model.getId());
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
      return models.stream()
              .sorted(comparator)
              .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String id) {
        return models
                .stream()
                .filter(filterById(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index > models.size() || models.isEmpty()) return null;
        return models.get(index);
    }

    @Override
    public M delete(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M deleteById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M deleteByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }

}
