package ru.krivotulov.tm.exception.system;

import ru.krivotulov.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! argument ``" + argument + "`` not supported...");
    }

}
