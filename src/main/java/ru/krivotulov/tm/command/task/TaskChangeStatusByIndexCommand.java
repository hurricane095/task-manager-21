package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;

/**
 * TaskChangeStatusByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-change-status-by-index";

    public static final String DESCRIPTION = "Change task status by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.readLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getTaskService().changeStatusByIndex(userId, index, status);
    }

}
