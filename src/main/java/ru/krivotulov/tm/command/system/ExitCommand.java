package ru.krivotulov.tm.command.system;

/**
 * ExitCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String DESCRIPTION = "Close application.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
