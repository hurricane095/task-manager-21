package ru.krivotulov.tm.command.projecttask;

import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.enumerated.Role;

/**
 * AbstractProjectTaskCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId(){
        return getAuthService().getUserId();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
