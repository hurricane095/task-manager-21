package ru.krivotulov.tm.command.projecttask;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * BindTaskToProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public class BindTaskToProjectCommand extends AbstractProjectTaskCommand {

    public static final String NAME = "bind-task-to-project";

    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.readLine();
        System.out.println("TASK ID: ");
        final String taskId = TerminalUtil.readLine();
        final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
