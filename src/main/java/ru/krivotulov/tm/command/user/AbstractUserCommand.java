package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IUserService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.model.User;

/**
 * AbstractUserCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractUserCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public void showUser(final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
