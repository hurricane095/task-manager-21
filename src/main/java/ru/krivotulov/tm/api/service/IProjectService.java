package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);


}
