package ru.krivotulov.tm.api.service;

/**
 * IServiceLocator
 *
 * @author Aleksey_Krivotulov
 */
public interface IServiceLocator {

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    IProjectService getProjectService();

    ILoggerService getLoggerService();

    IUserService getUserService();

    IAuthService getAuthService();

}
