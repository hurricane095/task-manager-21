package ru.krivotulov.tm.api.model;

/**
 * ICommand
 *
 * @author Aleksey_Krivotulov
 */
public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
