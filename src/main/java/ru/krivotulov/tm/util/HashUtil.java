package ru.krivotulov.tm.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * HashUtil
 *
 * @author Aleksey_Krivotulov
 */
public interface HashUtil {

    String SECRET = "1234";
    Integer ITERATION = 9876;

    static String salt(final String password) {
        if (password == null) return null;
        String result = password;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(final String val) {
        if (val == null) return null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(val.getBytes());
            final StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                stringBuffer.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
