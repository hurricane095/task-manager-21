package ru.krivotulov.tm.util;

import ru.krivotulov.tm.model.AbstractModel;

import java.util.Collection;

/**
 * ListUtil
 *
 * @author Aleksey_Krivotulov
 */
public interface ListUtil {

    static <T extends AbstractModel> Collection<T> emptyListToNull(Collection<T> collection) {
        return collection == null || collection.isEmpty() ? null : collection;
    }
}
