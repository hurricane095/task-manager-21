package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.exception.entity.TaskNotFoundException;
import ru.krivotulov.tm.exception.field.*;
import ru.krivotulov.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, String name, String description, Date dateBegin, Date dateEnd) {
        return Optional.ofNullable(create(userId, name, description))
                .map(task -> {
                    task.setDateBegin(dateBegin);
                    task.setDateEnd(dateEnd);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, taskId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(task -> {
                    task.setName(name);
                    task.setDescription(description);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(task -> {
                    task.setName(name);
                    task.setDescription(description);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(task -> {
                    task.setStatus(status);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(task -> {
                    task.setStatus(status);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

}
