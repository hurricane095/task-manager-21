package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.exception.entity.ProjectNotFoundException;
import ru.krivotulov.tm.exception.field.*;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.repository.AbstractRepository;
import ru.krivotulov.tm.repository.ProjectRepository;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, String name, String description, Date dateBegin, Date dateEnd) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return Optional.ofNullable(create(userId, name, description))
                .map(project -> {
                    project.setDateBegin(dateBegin);
                    project.setDateEnd(dateEnd);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);

    }

    @Override
    public Project updateById(final String userId, String id, String name, String description) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(project -> {
                    project.setName(name);
                    project.setDescription(description);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project updateByIndex(final String userId, Integer index, String name, String description) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(project -> {
                    project.setName(name);
                    project.setDescription(description);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project changeStatusById(final String userId, String id, Status status) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(project -> {
                    project.setStatus(status);
                    project.setUserId(userId);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project changeStatusByIndex(final String userId, Integer index, Status status) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(project -> {
                    project.setStatus(status);
                    project.setUserId(userId);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

}
